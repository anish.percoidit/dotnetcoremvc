﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreMVC.Models
{
    public class StudentContext : DbContext
    {
        //reason to do this: when we connect database, we will do it through SQL injection
        public StudentContext(DbContextOptions<StudentContext> options) : base(options)
        {
                
        }

        public DbSet<Student> Student { get; set; }
        public DbSet<Faculty> Faculty { get; set; }
        //inside Dbset:(Model) // then(TableName)

    }
}
