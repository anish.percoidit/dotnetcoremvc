﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreMVC.Models
{
    public class Faculty
    {
        [Key]
        public int ID { get; set; }

        public string FacultyName { get; set; }
    }
}
