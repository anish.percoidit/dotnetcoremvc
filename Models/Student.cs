﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreMVC.Models
{
    public class Student
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "Required*")]
        public string First_Name { get; set; }

        [Required(ErrorMessage = "Required*")]
        public string Last_Name { get; set; }

        [Required(ErrorMessage = "Required*")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required*")]
        [Display(Name = "Faculty")]
        public int FacultyID { get; set; }

        [Required(ErrorMessage = "Required*")]
        public string Mobile { get; set; }

        public string Description { get; set; }

        [NotMapped]
        public string FacultyName { get; set; }
    }
}
