﻿using DotNetCoreMVC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCoreMVC.Controllers
{
    public class StudentController : Controller
    {

        //instance of database (Dependency Injection)// just calling the StudentContext Property 
        private readonly StudentContext _Db;
        //initializing a constructor and passing the property of StudentContext(Db) to _Db(this class)
        public StudentController(StudentContext Db)
        {
            _Db = Db;
        }

        public IActionResult StudentList()
        {
            try
            {
                //var stdList = _Db.Student.ToList();//if we only had a single table

                var stdList = from a in _Db.Student
                              join b in _Db.Faculty
                              on a.FacultyID equals b.ID
                              into Fac
                              from b in Fac.DefaultIfEmpty()

                              select new Student
                            {
                                ID=a.ID,
                                First_Name=a.First_Name,
                                Last_Name=a.Last_Name,
                                Email=a.Email,
                                Mobile=a.Mobile,
                                Description=a.Description,
                                FacultyID=a.FacultyID,

                                  FacultyName=b==null?"":b.FacultyName
                              };

                return View(stdList);
            }
            catch(Exception ex)
            {
                return View();
            }    
        }

        public IActionResult Create(Student obj)
        {
            loadDDL(); 
            return View(obj);
        }

        [HttpPost]
        public async Task<IActionResult> AddStudent(Student obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (obj.ID == 0)
                    {
                        _Db.Student.Add(obj);
                        await _Db.SaveChangesAsync();
                    }
                    else
                    {
                        _Db.Entry(obj).State = EntityState.Modified;
                        await _Db.SaveChangesAsync();
                    }

                    return RedirectToAction("StudentList");
                }

                return View();
            }
            catch (Exception ex)
            {
                return RedirectToAction("StudentList");
            }
        }

        public async Task<IActionResult> DeleteStd(int id)
        {
            try
            {
                var std =await _Db.Student.FindAsync(id);
                if (std!=null)
                {
                    _Db.Student.Remove(std);
                    await _Db.SaveChangesAsync();
                }

                return RedirectToAction("StudentList");
            }
            catch(Exception ex)
            {
                return RedirectToAction("StudentList");
            }
        }

        private void loadDDL()
        {
            try
            {
                List<Faculty> facList = new List<Faculty>();
                facList = _Db.Faculty.ToList();
                facList.Insert(0, new Faculty { ID = 0, FacultyName = "Please Select" });

                ViewBag.FacList = facList;
            }
            catch(Exception ex)
            {

            }
        }

    }
}
